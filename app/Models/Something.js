'use strict'

const Model = use('Model')

class Something extends Model {
	static get table () {
		return 'something'
	}
}

module.exports = Something
