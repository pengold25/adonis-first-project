'use strict'

const Schema = use('Schema')

class TaskSchema extends Schema {
  up () {
    this.table('tasks', (table) => {
      table.dropColumn('title')
    })
  }

  down () {
    this.table('tasks', (table) => {
      table.string('title')
    })
  }
}

module.exports = TaskSchema
